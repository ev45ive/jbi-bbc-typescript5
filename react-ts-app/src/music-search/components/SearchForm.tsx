import React, { useEffect, useRef, useState } from "react";

interface Props {
  onSearch: (params: { query: string }) => void;
}

export const SearchForm = ({ onSearch }: Props) => {
  const [query, setQuery] = useState("batman");

  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  return (
    <div>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          value={query}
          ref={inputRef}
          onChange={(e) => setQuery(e.target.value)}
        />
        <button
          onClick={() => {
            onSearch({ query });
          }}
          className="btn btn-outline-secondary"
          type="button"
        >
          Search
        </button>
      </div>
    </div>
  );
};
