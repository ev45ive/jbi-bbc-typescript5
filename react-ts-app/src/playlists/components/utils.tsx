// cls("a", "b", Math.random() > 0.5 && "c");

export const cls = (...classes: (string | false)[]): string => {
  return (
    // (classes as string[])
    classes //  (string | false)[]
      // Custom type guard function - if true then arg has Type
      .filter((r): r is string => {
        // return r !== false;
        return typeof r === "string";
      })
      // Array<string> || string[]
      .join(" ")
  );
};
