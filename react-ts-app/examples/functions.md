## Function types

```ts
type AdderFn = (x: number, y: number, z?: number) => number;

interface AdderI {
  (x: number, y: number): void;
}

// let myAdd: (x: number, y: number) => number = function (x, y) {
let myAdd: AdderFn = function (x, y) {
  return x + y;
};
// let funcWithCallback = (cb: (x: number, y: number) => void) => {}
let funcWithCallback = (cb: AdderI) => {};
funcWithCallback(myAdd);
```

## Overloading function type (not implementation!!!)

```ts
interface SearchFunc {
  (source: string, subString: string): boolean;
  /**
   * Check if string is present startign from [startPos]
   */
  (source: string, whatToSearch: string, startPos: number): string; // overload
}

function mySearch(heap, needle, start: number = 0) {
  // arguments.length ???
  // if typeof heap == ???
  return heap.includes(needle, start);
}

mySearch("test", "t");

mySearch("test", "t", 3);

/* or */

function aSearchFunc(source: string, subString: string): boolean;
function aSearchFunc(source: string,whatToSearch: string,startPos: number): string; 
function aSearchFunc(heap, needle, start: number = 0) {
  return heap.includes(needle, start);
}

mySearch("test", "t");

mySearch("test", "t", 3);
```
