```ts

const album = {
  type: "album" as const,
  name: "album123",
  tracks_number: 22,
  tracks: [] as Track[]
};

// if(typeof 'x' === 'string'){}

type Album = typeof album;

const album2: Album = {
  type: "album",
  name: "album234",
  tracks_number: 22,
  tracks:[]
};


```