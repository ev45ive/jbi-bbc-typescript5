import React, { useState } from "react";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistEditor } from "../components/PlaylistEditor";
import { PlaylistList } from "../components/PlaylistList";
import { playlistsMocks } from "./playlistsMocks";


interface Props {}

type ViewModesEnum = "details" | "edit" | "create"; // | "review";

export const PlaylistsView = (props: Props) => {
  const [playlists, setPlaylists] = useState<Playlist[]>(playlistsMocks);
  const [selected, setSelected] = useState<Playlist>(/* undefined */);

  const [mode, setMode] = useState<ViewModesEnum>("details");

  const selectPlaylist = (id: Playlist["id"]) => {
    setSelected(playlists.find((p) => p.id === id));
  };

  const edit = () => {
    setMode("edit");
  };

  const cancel = () => {
    setMode("details");
    setSelected(undefined);
  };

  const create = () => {
    setMode("create");
  };

  const save = (draft: Playlist): string | null => {
    // setPlaylists(playlists) // mutable - wont triger update

    setPlaylists(playlists.map((p) => (p.id === draft.id ? draft : p)));
    setSelected(draft);
    setMode("details");

    return null;
  };

  const RenderMode = () => {
    switch (mode) {
      case "details":
        return <PlaylistDetails playlist={selected} onEdit={edit} />;
      case "edit":
        return (
          <PlaylistEditor playlist={selected} onCancel={cancel} onSave={save} />
        );
      case "create":
        return <PlaylistEditor onCancel={cancel} onSave={save} />;
      default:
        const _never: never = mode;
        throw "Unsupported mode " + mode;
    }
  };

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistList
            onSelect={selectPlaylist}
            playlists={playlists}
            selectedId={selected?.id}
          />
          <button className="btn btn-info float-end" onClick={create}>
            Create new playlist
          </button>
        </div>
        <div className="col">
          <RenderMode />
        </div>
      </div>
    </div>
  );
};
