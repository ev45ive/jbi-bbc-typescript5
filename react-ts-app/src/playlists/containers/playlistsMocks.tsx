import { Playlist } from "../../core/model/Playlist";

export const playlistsMocks: Playlist[] = [
  {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "best playlist 123",
  },
  {
    id: "234",
    name: "Playlist 234",
    public: true,
    description: "best playlist 234",
  },
  {
    id: "345",
    name: "Playlist 345",
    public: true,
    description: "best playlist 345",
  },
];
