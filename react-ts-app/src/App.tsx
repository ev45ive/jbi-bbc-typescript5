import React, { useEffect, useState } from "react";
import logo from "./logo.svg";
import "bootstrap/dist/css/bootstrap.css";
import { PlaylistsView } from "./playlists/containers/PlaylistsView";
import {
  assertUserProfile,
  isUserProfile,
  UserProfile,
} from "./core/model/UserProfile";
import axios from "axios";
import {
  authService,
  IManageSession,
  IProvideAccessToken,
  userService,
} from "./core/services/services";
import MusicSearchView from "./music-search/containers/MusicSearchView";

const auth: IProvideAccessToken & IManageSession = authService;

function App() {
  const [user, setUser] = useState<UserProfile | null>(null);

  useEffect(() => {
    userService
      .getUser()
      .then((user) => setUser(user))
      .catch(console.error);
  }, []);

  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <div className="container">
          <a className="navbar-brand" href="#">
            Navbar
          </a>
          <button className="navbar-toggler" type="button">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Features
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Pricing
                </a>
              </li>
            </ul>
            <div className="ms-auto navbar-text">
              {/* <div>Welcome Guest | Login</div> */}
              {user ? (
                <div onClick={() => auth.logout()}>
                  Welcome {user.display_name} | Logout
                </div>
              ) : (
                <div onClick={() => auth.login()}>Welcome Guest | Login</div>
              )}
            </div>
          </div>
        </div>
      </nav>

      <div className="">
        {/* .container>.row>.col>h1{MusicApp} */}
        <div className="container">
          <div className="row">
            <div className="col">
              <h1>MusicApp</h1>
              <MusicSearchView />
              {/* <PlaylistsView /> */}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
