
## Generics

```ts

const x1: string[] = ["a", "b", "c"];
const x2: Array<string> = ["a", "b", "c"];
const x3: Array<string | number> = ["a", "b", 123, "x"];
x2.pop(); // string | undefined
// x2.push(123) // Error

const tictactoe: Array<Array<"X" | "O">> = [
  ["O", "X", "O"],
  ["X", "O", "O"],
  ["O", "X", "X"],
];
/// ---
type RefObject<T> = {
  current: T | null;
  getValue<T>(): T | null;
};

function getValue<T>(this: RefObject<T>) {
  return this.current;
}

const ref1: RefObject<number> = { current: 123, getValue };
const ref2: RefObject<string> = { current: "aaa", getValue };

/// --- Useless Generics? ( documentation? )

declare function parse<T>(params: string): T;
declare function serialize<T>(params: T): string;

parse<string[]>("test");
parse("test") as string[];
serialize({} as Object).toLocaleLowerCase();

// ---

function identity<T>(x: T): T {
  return x;
}

// function takeFirst(arr: any) {
//   return arr[0];
// }
// const x: number = takeFirst(["as", "b", "c"]);

// function takeFirst(arr: Array<string>): string {
// function takeFirst(arr: Array<number>): number {
// function takeFirst<T>(arr: Array<T>): T {
function takeFirst<T>(arr: T[]): T {
  return arr[0];
}
const a1: number = takeFirst<number>([1, 2, 3]); // takeFirst(arr: number[]): number
const a2: never = takeFirst([]); // takeFirst(arr: never[]): never
const a3: number = takeFirst([1, 2, 3]); // takeFirst(arr: never[]): never
const a4: string | number = takeFirst([1, "2"]); // takeFirst<string | number>(arr: (string | number)[]): string | number

// ---
class Queue<ItemT> {
  constructor(private data: ItemT[] = []) {}

  push(item: ItemT) {
    this.data.push(item);
  }
  pop(): ItemT | undefined {
    return this.data.shift();
  }
}
// const q = new Queue<string>();
const q = new Queue(["12"]);

q.push("123");
const n: string | undefined = q.pop();

// ----

// type ObjWithName = { name: string };
// function printName<T extends ObjWithName>(arg: T) {

function printName<T extends { name: string }>(arg: T) {
  return arg.name;
}

printName({ name: "Kate" }); // enough - OK
printName({ name: "Michael", age: 22 }); // more is OK too
// printName({ age: 22 }); // Error!

// ---
interface SomeObj {
  name: string;
  age: number;
}
type keys = keyof SomeObj;
const k: keys = "age"; // 'name' | 'age'

// function getKey<T extends {}, K extends string>(obj: T, key: K) {
// function getKey<T extends { name:string, age:number }, K extends 'name' | 'age'>(obj: T, key: K) {
// function getKey<T extends { name:string, age:number }, K extends 'name' | 'age'>(obj: T, key: K) {
function getKey<T extends {}, K extends keyof T>(obj: T, key: K): T[K] {
  return obj[key];
}
const x: string = getKey({ abc: "123" }, "abc");

class Person{}
const alice = new Person()
type alice = typeof alice
type aliceConstrucor = typeof Person

// --
enum Role {
  admin = "admin",
  user = "user",
}
type roles = keyof typeof Role;
const role: roles = "admin";

```