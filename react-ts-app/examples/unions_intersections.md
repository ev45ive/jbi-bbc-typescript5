# Unions and Intersections

## Types and interfaces


https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#union-types

```ts

type A = {
  a: string;
  b: number;
  // x: number;
};
type B = {
  b: number;
  c: string;
  x: string;
};
type Union = A | B;
type Intersection = A & B;

/* Intersections */

let i1: Intersection = {
  a: "a",
  b: 1,
  c: "c",
  // x :'1' // never
};
i1.a;

/* Unions */
let x1: Union = { a: "1", b: 1, x: 12 };
x1.a;
// x1.c // errror
const x2: Union = { /*   */ b: 1, c: "1", x: "12" };
x2.c;

function x(param: Union) {
  // param.a // error
  param.b;
  param.x.toString()

  if ("a" in param) {
    param.a;
    param.x = 213
  } else {
    param.c;
    param.x = 'x'
  }
}
```