
## GIT 
cd ..
git clone https://bitbucket.org/ev45ive/jbi-bbc-typescript5.git jbi-bbc-typescript5
cd jbi-bbc-typescript5/react-ts-app
npm i 
npm start

## Update
git stash -u
git pull 
<!-- git pull -u origin master -->

## Agenda
https://www.jbinternational.co.uk/course/771/typescript-advanced-training-course-class-london-uk 

## Installations
node -v 
v14.17.0 

npm -v 
6.14.6

code -v 
1.62.3
ccbaa2d27e38e5afa3e5c21c1c7bef4657064247
x64

git --version
git version 2.31.1.windows.1

Google Chrome	96.0.4664.45 

npm i -g typescript

tsc --version
Version 4.5.2

## Extensions
React snippets
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

quicktype
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## TSC
tsc file.ts
tsc file.ts
tsc migration/index.ts --outDir build
tsc --module exnext  migration/index.ts --outDir build

tsc --strict --allowJS  migration/index.ts --outDir build
tsc --strict --allowJS  migration/lib.js --outDir build

//  Parameter 'a' implicitly has an 'any' type.

## TsConfig
cd ./migration
tsc --strict --allowJs --init  index.ts

https://www.typescriptlang.org/tsconfig

<!-- Created a new tsconfig.json with:                                                                                       

  target: es2016
  module: commonjs
  strict: true
  esModuleInterop: true
  skipLibCheck: true
  forceConsistentCasingInFileNames: true 

You can learn more at https://aka.ms/tsconfig.json -->

## Declaration - Ambient types
tsc --allowJs --declaration --emitDeclarationOnly lib.js
=> lib.d.ts 

## Frontend project
npm init -y
tsc --init --strict


## Webpack

npm install webpack webpack-cli --save-dev

npx webpack init ./

? Which of the following JS solutions do you want to use? Typescript
? Do you want to use webpack-dev-server? Yes
? Do you want to simplify the creation of HTML files for your bundle? Yes
? Do you want to add PWA support? No
? Which of the following CSS solutions do you want to use? CSS only
? Will you be using PostCSS in your project? No
? Do you want to extract CSS for every file? Only for Production
? Do you like to install prettier to format generated configuration? No
? Pick a package manager: npm

[webpack-cli] ℹ INFO  Initialising project...

 conflict package.json
? Overwrite package.json? overwrite
    force package.json
 conflict src\index.ts
? Overwrite src\index.ts? do not overwrite
     skip src\index.ts
   create README.md
   create index.html
   create webpack.config.js
 conflict tsconfig.json
? Overwrite tsconfig.json? do not overwrite
     skip tsconfig.json

## Nodejs TS backend
npm i @types/node

npm i -g nodemon

npm i typescript ts-node ts-node-dev

## Node debugger

node --inspect-brk dist/index.js
% Debugger listening on ws://127.0.0.1:9229/89fe0bf1-a287-45d8-ac15-0f0a7882fa41
For help, see: https://nodejs.org/en/docs/inspector
Debugger attached.

node --inspect dist/index.js

## React 
npx create-react-app react-ts-app --template typescript --use-npm


## Playlists Module

mkdir -p src/playlists/containers
mkdir -p src/playlists/components

touch src/playlists/containers/PlaylistsView.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistEditor.tsx


## Type narrowing, pattern matching

https://www.typescriptlang.org/docs/handbook/2/narrowing.html#exhaustiveness-checking

https://github.com/colinhacks/zod#unions

https://github.com/gvergnaud/ts-pattern


## MISSING TYPES!?
https://definitelytyped.org/

npm i --save-dev @types/your-module-name

https://www.typescriptlang.org/docs/handbook/modules.html#ambient-modules



## Music search module


mkdir -p src/music-search/containers
mkdir -p src/music-search/components

touch src/music-search/containers/MusicSearchView.tsx
touch src/music-search/components/SearchForm.tsx
touch src/music-search/components/SearchResults.tsx
touch src/music-search/components/AlbumCard.tsx

<!-- touch src/music-search/components/ArtistCard.tsx -->