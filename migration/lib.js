// @ts-check

/**
 * Function that add numbers
 *
 * @export
 * @param {number} a
 * @param {number} b
 * @return {number} a result
 */
export function add(a, b) {
    return a + b;
}

// @ts-ignore
export function substract(a, b) {
    return a - b
}

// @ts-expect-error
export function multiply(a, b) {
    return a * b
}

// @ts-expect-error
add('1','2')