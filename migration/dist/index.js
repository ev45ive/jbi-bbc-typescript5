"use strict";
/// <reference path="./index.d.ts"/>
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.special = void 0;
const _lib_1 = require("@lib");
const jquery_1 = __importDefault(require("jquery"));
exports.special = { abc: 123 };
jquery_1.default.ajax; // npm i @types/jquery
jquery_1.default.ajax(1); // overload index.d.ts
jquery_1.default.myPlugin = (x) => { }; // declaration merging index.d.ts
let y = (0, _lib_1.substract)(123, 123);
// let y : number = add('1', '123')
let x = (0, _lib_1.add)(1, 2);
x = 123;
// x = '234'
// x = {}
// x = [{ x: 123 }]
// x.get.me.a.million.pounds().now()
const arrowFunc = () => { };
class SomeClass {
}
window.document.body;
MouseEvent;
