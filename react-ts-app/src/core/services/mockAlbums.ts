import { Album } from "./services";

// const searchService = new SearchService(authService)
export const mockAlbums: Album[] = [
    {
        id: '123',
        type: 'album',
        name: 'album 123',
        images: [
            { url: 'https://www.placecage.com/c/300/300' }
        ]
    },
    {
        id: '234',
        type: 'album',
        name: 'album 234',
        images: [
            { url: 'https://www.placecage.com/c/200/200' }
        ]
    },
    {
        id: '345',
        type: 'album',
        name: 'album 345',
        images: [
            { url: 'https://www.placecage.com/c/300/300' }
        ]
    },
    {
        id: '456',
        type: 'album',
        name: 'album 456',
        images: [
            { url: 'https://www.placecage.com/c/400/400' }
        ]
    },
];
