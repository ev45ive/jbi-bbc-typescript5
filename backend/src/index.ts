import http from 'http'

// function add(x, y) { } // noImplicitAny
// function takesomething(something: string | null = null) {  // strictNullChecks
//     if (something !== null) {
//         something.toString()
//     }
// }


const server = http.createServer((req: http.IncomingMessage, res: http.ServerResponse) => {
    res.write('<h1>Hello Nodemon</h1>')
    res.end()
})

server.listen(3000, () => {
    console.log('Server is listening on http://localhost:3000/ ');
})

// window.document.body // TS thinks its in Browser Environment!