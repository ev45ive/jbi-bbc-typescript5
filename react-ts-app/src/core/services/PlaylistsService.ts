import { createEmptyPlaylist } from "../../playlists/components/PlaylistEditor"
import { playlistsMocks } from "../../playlists/containers/playlistsMocks"
import { Playlist } from "../model/Playlist"


// playlistsMocks

/**
 * Returns all playlists
 */
export const fetchAllPlaylists = (): Playlist[] => {
    return playlistsMocks
}

/**
 * Returns playlist
 */
export const fetchPlaylistById = (id: Playlist['id']): Playlist | undefined => {
    return playlistsMocks.find(p => p.id === id)
}
// export const fetchPlaylistById2 = (id: Playlist['id']): Playlist => {
//     const result = playlistsMocks.find(p => p.id === id)
//     if (!result) throw Error('Not Found')
//     return result
// }

/**
 * Finds all playlists with name
 */
const findPlaylistsByName = (name: Playlist['name']): Playlist[] => {
    return playlistsMocks.filter(p => p.name.includes(name))
}

/**
 * Finds all playlists with Description
 */
const findPlaylistsByDescription = (description: Playlist['description']): Playlist[] => {
    return playlistsMocks.filter(p => p.description.includes(description))
}

type Criteria =
    | { name: string }
    | { description: string }
    | { id: string }

// export function findPlaylistsBy(criteria: { public: string }): Playlist[]
export function findPlaylistsBy(criteria: { name: string }): Playlist[]
export function findPlaylistsBy(criteria: { description: string }): Playlist[]
/** search one by id */
export function findPlaylistsBy(criteria: { id: string }): Playlist | undefined
export function findPlaylistsBy(criteria: Criteria): Playlist[] | Playlist | undefined {
    if ('name' in criteria) {
        return findPlaylistsByName(criteria.name)
    } else if ('description' in criteria) {
        return findPlaylistsByDescription(criteria.description)
    } else if ('id' in criteria) {
        return fetchPlaylistById(criteria.id)
    } else {
        const never: never = criteria;
        throw 'Unsuported criteria'
    }
}
// const result1 = findPlaylistsBy({name:'Alice'})
// const result2 = findPlaylistsBy({description:'best playlist'})
// const result3 = findPlaylistsBy({id:'123'})

/**
 * Delete  playlist
 */
export const deletePlaylist = (id: Playlist['id']) => {
    const index = playlistsMocks.findIndex(p => p.id === id)
    delete playlistsMocks[index]
}

// interface PlaylistDTO {
//     readonly name: Playlist['name'];
//     readonly public?: Playlist['public'];
//     readonly description?: Playlist['description'];
//     dateCreated?: Date
// }

// type PlaylistDTO = {
//     // [key: string]: string
//     // [key: string]: Playlist[key]
//     readonly [key in keyof Playlist]?: Playlist[key]
// }

// type Partial<T> = {
//     [key in keyof T]?: T[key]
// }

// type Readonly<T> = {
//     readonly [key in keyof T]: T[key]
// }

// type Pick<T , K extends keyof T> = {
//     [P in K]: T[P]
// }

// type PlaylistDTO = Readonly<Partial<Playlist>>
// type PlaylistDTO = Readonly<Partial<Pick<Playlist, 'name' | 'description' | 'public'>>>

// type AllButId = Exclude<keyof Playlist, 'id'>
// type PlaylistDTO = Readonly<Partial<Pick<Playlist, AllButId>>>

// type CreateDTO<T> = Readonly<Partial<Pick<T, Exclude<keyof T, 'id'>>>>
type CreateDTO<T> = Readonly<Partial<Omit<T, 'id'>>>
type PlaylistDTO = CreateDTO<Playlist>

/**
 * Update  playlist
 */
export const updatePlaylist = (id: Playlist['id'], data: PlaylistDTO) => {
    const index = playlistsMocks.findIndex(p => p.id === id)
    const updated = { ...playlistsMocks[index], ...data }
    playlistsMocks[index] = updated
    return updated
}


type newPlaylistT = ReturnType<typeof updatePlaylist> 

/**
 * Create playlist
 *   createPlaylist({
 *       name: 'New name'
 *    })
 *  */
export const createPlaylist = (data: PlaylistDTO) => {
    if ('id' in data) {
        throw 'Cannot create playlist with existing ID'
    }
    const draft: Playlist = {
        ...createEmptyPlaylist(),
        ...data,
        id: (100_000 * Math.random()).toFixed(),
    }
    playlistsMocks.push(draft)
    return draft
}

export enum SortDir {
    ASC = 1, DESC = -1, DEFAULT = SortDir.ASC
}
// DIR[1] == 'ASC'
// DIR['ASC'] == 1

export enum SortKey {
    name = 'name',
    // pancakes = 'pancakes'
    // public = 'public',
    description = 'description',
    // DEFAULT = SortKey.name // Computed values are not permitted in an enum with string valued members.
}

// function createSorter(key, direction: 'ASC' | 'DESC') {
class Sorter {
    // key: SortKey  

    constructor(private key: SortKey = SortKey.name) {
        // this.key = key
    }

    direction: SortDir = SortDir.DEFAULT

    getSorter() {
        // this.direction //  ok 

        // return function ( a: Playlist, b: Playlist) {
        //      this  // 'this' implicitly has type 'any' because it does not have a type annotation.

        // return function (this: Sorter, a: Playlist, b: Playlist) {
        //     const akey = a[this.key]
        //     const bkey = b[this.key]
        //     return akey.localeCompare(bkey, 'en-EN', {}) * this.direction
        // }.bind(this)

        return (a: Playlist, b: Playlist) => {
            const akey = a[this.key]
            const bkey = b[this.key]
            return akey.localeCompare(bkey, 'en-EN', {}) * this.direction
        }
    }
}

const sorter = new Sorter(SortKey.name)

playlistsMocks.sort(sorter.getSorter())