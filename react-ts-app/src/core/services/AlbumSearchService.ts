import axios from "axios";
import { object, z } from "zod";
import { Artist } from "../model/Artist";
import { mockAlbums } from "./mockAlbums";
import { ISearchAlbums, Album, authService } from "./services";


export type SearchParams = {
    query: string;
    type: 'artist',
} | {
    query: string;
    type: 'album',
}

export class AlbumSearchService {

    search = async (params: SearchParams) => {
        switch (params.type) {
            case 'album': return this.searchAlbum(params)
            case 'artist': return this.searchArtist(params)
        }
    }

    async searchArtist({ query }: {
        query: string;
        type: 'artist',
    }): Promise<Artist[]> {
        const resp = await axios.get<SearchResponseArtist>('https://api.spotify.com/v1/search', {
            headers: {
                Authorization: `Bearer ${authService.getAccessToken()}`,
            },
            params: {
                type: 'artist',
                query
            }
        })

        return resp.data.artists.items
    }

    async searchAlbum({ query }: {
        query: string;
        type: 'album',
    }): Promise<Album[]> {
        try {
            const resp = await axios.get<unknown>('https://api.spotify.com/v1/search', {
                headers: {
                    Authorization: `Bearer ${authService.getAccessToken()}`,
                },
                params: {
                    type: 'album',
                    query
                }
            })
            assertSearchAlbumsResponse(resp.data)

            return resp.data.albums.items
        } catch (error: unknown) {
            if (!axios.isAxiosError(error)) throw new Error('Unexpected error')
            assertSpotifyError(error.response?.data)
            if (error.response.status === 401) {
                authService.login()
            }

            throw Error(error.response.data.error.message)
        }
    }
}

function assertSpotifyError(data: any): asserts data is SpotifyError {
    if (!('error' in data && 'message' in data.error)) {
        throw 'Unexpected server error'
    }
}

export interface SpotifyError {
    error: {
        message: string
        status: number
    }
}

// const searchrespSchema = z.object({
//     albums: z.object({
//         items: z.array(z.object({
//             type: z.literal('album')
//         }))
//     })
// })
// searchrespSchema.parse(data)

function assertSearchAlbumsResponse(data: any): asserts data is SearchResponseAlbum {
    if ('albums' in data
        && 'items' in data.albums
        && Array.isArray(data.albums.items)
    ) return;

    throw new Error('Invalid search response')
}

// https://developer.spotify.com/documentation/web-api/reference/#/operations/search

interface SearchResponseAlbum {
    albums: PagingObject<Album>
}

interface SearchResponseArtist {
    artists: PagingObject<Artist>
}

// interface SearchResponse<T, K> {
//     artists: PagingObject<T>
// }


export interface PagingObject<T> {
    href: string;
    items: T[];
    limit: number;
    next: string;
    offset: number;
    previous: string;
    total: number;
}
