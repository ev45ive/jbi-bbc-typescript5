
export interface Track {
  type: "track";
  id: string;
  name: string;
  duration_ms: number;
  explicit: boolean;
}
