import React, { useState } from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist?: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => string | null;
}

export const createEmptyPlaylist = (): Playlist => ({
  id: "",
  public: false,
  name: "",
  description: "",
});

export const PlaylistEditor = ({
  playlist = createEmptyPlaylist(), // only if playlist === undefined
  onCancel,
  onSave,
}: Props) => {
  // const playlist3 = playlist === undefined ? createEmptyPlaylist() : playlist;
  // const playlist4 = playlist ?? createEmptyPlaylist()

  const [playlistName, setPlaylistName] = useState(playlist.name);
  const [isPublic, setIsPublic] = useState(playlist.public);
  const [playlistDescription, setPlaylistDescription] = useState(
    playlist.description
  );

  // const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  const handleNameChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    setPlaylistName(event.target.value);
  };

  const handlePublicChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    setIsPublic(event.target.checked);
  };

  const submit = () => {
    // playlist.name = '123' // mutable - wont triger update
    // onSave(playlist) // mutable - wont triger update

    const result = onSave({
      //   id: playlist.id,
      ...playlist,
      name: playlistName,
      public: isPublic,
      description: playlistDescription,
    });
  };

  return (
    <div>
      {/* <pancakes sauce="banana"/> */}
      PlaylistEditor
      <div className="form-group mb-3">
        <label htmlFor="playlist_name">Name:</label>
        <input
          type="text"
          className="form-control"
          name="playlist_name"
          id="playlist_name"
          placeholder="Name"
          onChange={handleNameChange}
          value={playlistName}
        />
        <small id="helpId"> {playlistName.length} / 170</small>
      </div>
      <div className="form-check mb-3">
        <label className="form-check-label">
          <input
            type="checkbox"
            className="form-check-input"
            name="playlist_public"
            id="playlist_public"
            checked={isPublic}
            onChange={handlePublicChange}
          />
          Public
        </label>
      </div>
      <div className="form-group mb-3">
        <label htmlFor="playlist_description">Description:</label>
        <textarea
          className="form-control"
          name="playlist_description"
          id="playlist_description"
          rows={3}
          value={playlistDescription}
          onChange={(event) => setPlaylistDescription(event.target.value)}
        ></textarea>
      </div>
      <button className="btn btn-danger mt-3" onClick={onCancel}>
        Cancel
      </button>
      <button className="btn btn-success mt-3" onClick={submit}>
        Save
      </button>
    </div>
  );
};

// PlaylistEditor.defaultProps = {
//   playlist: createEmptyPlaylist()
// }
