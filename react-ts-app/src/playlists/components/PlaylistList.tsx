import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "./utils";

interface Props {
  // selectedId: string;
  playlists: Playlist[];
  // selectedId: Playlist["id"] | undefined;
  selectedId?: Playlist["id"] ;
  onSelect: (id: Playlist["id"]) => void;
}

export const PlaylistList = ({ playlists, selectedId, onSelect }: Props) => {
  return (
    <div>
      <div className="list-group">
        {playlists.map((playlist, index) => (
          <div
            key={playlist.id}
            onClick={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
              onSelect(playlist.id);
            }}
            className={cls(
              "list-group-item ",
              selectedId === playlist.id && "active"
            )}
          >
            {index + 1}. {playlist.name}
          </div>
        ))}
      </div>
    </div>
  );
};
