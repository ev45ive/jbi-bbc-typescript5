import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Menu } from "./core/components/Menu";
import { authService } from "./core/services/services";
import reportWebVitals from "./reportWebVitals";

authService.init()

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// declare module "./core/components/Menu" {
//   export namespace Menu {
//     interface MenuProps {
//       x: 1;
//     }

//     export let SpecialItem: () => React.ReactElement;
//   }
// }
// Menu.SpecialItem = () => <div></div>;

// Menu.Options.setOption(13);

// const menu = (
//   <Menu.List x={1}>
//     <Menu.Item>1</Menu.Item>
//     <Menu.Item>2</Menu.Item>
//     <Menu.Item>3</Menu.Item>
//     <Menu.SpecialItem></Menu.SpecialItem>
//   </Menu.List>
// );

// declare global {
//   namespace JSX {
//     interface IntrinsicElements {
//       pancakes: React.DetailedHTMLProps<
//         React.HTMLAttributes<HTMLDivElement> & {
//           sauce: string;
//         },
//         HTMLDivElement
//       >;
//     }
//   }
// }
