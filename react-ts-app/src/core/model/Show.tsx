import { Track } from "./Track";

export interface Show {
  type: "show";
  id: string;
  name: string;
  description: string;
  episodes: Track[];
}
