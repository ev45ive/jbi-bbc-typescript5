import { mockAlbums } from "./mockAlbums";
import { ISearchAlbums, Album } from "./services";

export class MockSearchService implements ISearchAlbums {

    search(params: { query: string; }) {
        return Promise.resolve(mockAlbums)
    }
}
