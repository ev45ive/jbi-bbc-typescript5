import axios from "axios";
import { assertUserProfile, UserProfile } from "../model/UserProfile";
import { IProvideUser, IProvideAccessToken, authService } from "./services";

export class UserService implements IProvideUser {
    // private auth: IProvideAccessToken
    constructor(private auth: IProvideAccessToken) {
        // this.auth = auth
    }

    // Array<UserProfile>
    getUser(): Promise<UserProfile> {

        return axios.get<unknown>("https://api.spotify.com/v1/me", {
            headers: {
                Authorization: `Bearer ${authService.getAccessToken()}`,
            },
        })
            .then(resp => {
                assertUserProfile(resp.data);
                return resp.data
            })
            .catch(error => {
                // return Promise.reject(new Error(error.message))
                throw (new Error(error.message))
            })
    }

    async getUser2(): Promise<UserProfile> {

        const { data } = await axios.get<unknown>("https://api.spotify.com/v1/me", {
            headers: {
                Authorization: `Bearer ${authService.getAccessToken()}`,
            },
        })
        assertUserProfile(data);

        return data
    }

}
