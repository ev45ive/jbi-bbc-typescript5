import React from "react";
import { Artist } from "../../core/model/Artist";

interface Props {
  result: Artist;
}

export const ArtistCard = ({ result }: Props) => {
  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{result.name}</h5>
      </div>
    </div>
  );
};
//   <img src={result.images[0].url} className="card-img-top" alt="..." />
