/// <reference path="./index.d.ts"/>

import { add, substract } from "./lib.js";
import jQuery from 'jquery'

interface MySpecialType {
    abc: 123
}
export const special: MySpecialType = { abc: 123 }

jQuery.ajax()
jQuery.ajax // npm i @types/jquery
jQuery.ajax(1) // overload index.d.ts
jQuery.myPlugin = (x: number) => { } // declaration merging index.d.ts


let y = substract(123, 123)

// let y : number = add('1', '123')

let x = add(1, 2)
x = 123

// x = '234'
// x = {}
// x = [{ x: 123 }]
// x.get.me.a.million.pounds().now()

const arrowFunc = () => { }

class SomeClass { }

window.document.body
MouseEvent