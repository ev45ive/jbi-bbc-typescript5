import React, { ReactElement, useEffect, useState } from "react";
import { string } from "zod";
import { Artist } from "../../core/model/Artist";
import { SearchParams } from "../../core/services/AlbumSearchService";
import { mockAlbums } from "../../core/services/mockAlbums";
import {
  Album,
  ISearchAlbums,
  searchService,
} from "../../core/services/services";
import { SearchForm } from "../components/SearchForm";
import { SearchResults } from "../components/SearchResults";

interface Props {}

// Custom hook
function useSearch<R, P>(resolver: (params: P) => Promise<R>, params: P) {
  const [results, setResults] = useState<R | null>(null);
  const [message, setMessage] = useState("");

  useEffect(() => {
    setResults(null);
    resolver(params)
      .then((results) => {
        setResults(results);
      })
      .catch((error: Error) => {
        setMessage(error.message);
      });
  }, [params]);

  return { results, message } as const;
}

export default function MusicSearchView({}: Props): ReactElement {
  const [params, setParams] = useState<SearchParams>({
    query: "batman",
    type: "album",
  });

  const { results, message } = useSearch(searchService.search, params);

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm
            onSearch={(p) =>
              setParams({
                ...params,
                query: p.query,
              })
            }
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {message && <p className="alert alert-danger">{message}</p>}
          <SearchResults results={results || []} />
        </div>
      </div>
    </div>
  );
}
