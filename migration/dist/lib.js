"use strict";
// @ts-check
Object.defineProperty(exports, "__esModule", { value: true });
exports.multiply = exports.substract = exports.add = void 0;
/**
 * Function that add numbers
 *
 * @export
 * @param {number} a
 * @param {number} b
 * @return {number} a result
 */
function add(a, b) {
    return a + b;
}
exports.add = add;
// @ts-ignore
function substract(a, b) {
    return a - b;
}
exports.substract = substract;
// @ts-expect-error
function multiply(a, b) {
    return a * b;
}
exports.multiply = multiply;
// @ts-expect-error
add('1', '2');
