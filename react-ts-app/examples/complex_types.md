
## Interface vs type
https://stackoverflow.com/questions/37233735/interfaces-vs-types-in-typescript

https://pawelgrzybek.com/typescript-interface-vs-type/

https://blog.logrocket.com/types-vs-interfaces-in-typescript/



## Structural Typing - ( Ducktyping ) - vs Nominal Typing

interface Point { x: number; y:number }
interface Vector { x: number; y:number, length:number }

let p: Point = { x: 1, y: 2 };
let v: Vector = { x: 1, y: 2, length: 3 };

// p = v  // OK 
// v = p // Property 'length' is missing in type 'Point' but required in type 'Vector'.


// https://basarat.gitbook.io/typescript/main-1/nominaltyping
// https://betterprogramming.pub/nominal-typescript-eee36e9432d2

## primitives vs objects

```ts
// declare function create(o: Object): void;
// declare function create(o: object): void;
declare function create(o: object | null ): void;
// OK
create({ prop: 0 });
create(null);

// create(42); // Erorr
```


## Arrays

```ts
let list1: number[] = [1, 2, 3];
let list2: Array<number> = [1, 2, 3];
list2[3] = 2213;
// list2.push('123') // Argument of type 'string' is not assignable to parameter of type 'number'

// This is NOT and Array! This is a tuple!
let notAList: [number] = [1];
// notAList = [1,2,3] //  Source has 3 element(s) but target allows only 1

```

## Tuples

```ts

let person: [id: number, name: string];
person = [123, "John"];

function showPerson(person: [id: number, name: string]) {}
showPerson([123, "Test"]);

type PersonArgsTuple = [id: number, name: string];
function showPerson2(...[id, name]: PersonArgsTuple) {
  id.toFixed();
  name.toLocaleLowerCase();
}

const arr = [1, "2", true]; // (string | number | boolean)[]
arr.push(true, "test", 123);

let tuple1: [number, string, boolean] = [1, "2", true];
let tuple2 = [1, "2", true] as const

```


## Index signature 

```ts

interface CachePlaylist {
  [playlist_id: Playlist["id"]]: Playlist;
}

const p1 = {} as Playlist;

const playlistCache: CachePlaylist = {
  ["" + p1.id]: p1,
};

const p2 = {} as Playlist;
playlistCache[p2.id] = p2;

// delete p1.description // not optional!  (JS engine deoptimises hidden classes)

```