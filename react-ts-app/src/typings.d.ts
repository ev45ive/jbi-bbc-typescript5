
declare global {
    namespace JSX {
        interface IntrinsicElements {
            pancakes: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;
        }
    }
}