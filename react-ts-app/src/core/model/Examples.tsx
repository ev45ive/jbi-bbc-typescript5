/* ================= */

import { Show } from "./Show";
import { Track } from "./Track";

/* ================= */

export {};
interface Playlist {
  type: "playlist";
  id: string;
  name: string;
  public: boolean;
  description: string;
}

interface Playlist {
  /**
   * List of playlist tracks
   */
  tracks?: Track[];
}

function isPlaylist(res: any): res is Playlist {
  return "type" in res && res.type === "playlist";
}

function assertPlaylist(res: any): asserts res is Playlist {
  if (!("type" in res && res.type === "playlist")) {
    throw "No a playlist";
  }
}

function handleResponse2(res: any) {
  assertPlaylist(res);

  res.tracks;
}

function handleResponse(res: any) {
  if (isPlaylist(res)) {
    // true =>
    res.tracks;
  } else {
    // false =>
    res; // any
  }
}

export function getLabelByTag(result: Playlist | Track | Show): string {
  switch (result.type) {
    case "playlist":
      return `${result.name} (${result.tracks || 0} tracks)`;
    case "show":
      return `${result.name} (${result.episodes} episodes)`;
    case "track":
      return `${result.name} (${result.duration_ms} sec)`;
    default:
      exhaustivenessCheck(result);
  }
}

const somePlaylist = {
  id: "123",
  name: "Playlist 123",
  public: true,
  description: "best playlist 123",
};
type APlaylist = typeof somePlaylist;

function getLabel(result: Playlist | Track | Show): string {
  // if( typeof result == '' ){
  // if( result instanceof Playlist ) // 'Playlist' only refers to a type, but is being used as a value here.

  if ("public" in result) {
    return `${result.name} (${result.tracks || 0} tracks)`;
  }
  if ("duration_ms" in result) {
    return `${result.name} (${result.duration_ms} sec)`;
  }
  if ("episodes" in result) {
    return `${result.name} (${result.episodes} episodes)`;
  }
  exhaustivenessCheck(result);
}

function parseData(data: string | undefined) {
  (data as any).toLocaleLowerCase();
  (data as string).toLocaleLowerCase();

  if (data !== undefined) {
    data.toLocaleLowerCase();
  } else {
    data;
  }

  const res1 = data !== undefined && data.toLocaleLowerCase(); // string | false
  const res2 = data && data.toLocaleLowerCase(); // string | undefined

  const res3 = data ? data.toLocaleLowerCase() : "No Data"; // string
  const res4 = (data && data.toLocaleLowerCase()) || "No Data"; // string
  const res5 = data ?? "No Data"; // string

  const res6 = data && data.toLocaleLowerCase(); // string | undefined
  const res7 = data?.toLocaleLowerCase() || "No Data"; // string

  // conditional operator vs non-optional assertion
  const res8 = data?.toLocaleLowerCase(); // string | undefined  // Good!
  const res9 = data!.toLocaleLowerCase(); // string              // Bad!
  const res10 = (data as string).toLocaleLowerCase(); // string  // Bad!
}

function parsePrice(price: number | string): string {
  // return price.toString() // toString() exists on both number and string - safe

  if (typeof price === "number") {
    return price.toFixed(2);
  }
  if (typeof price === "string") {
    return parseFloat(price).toFixed(2);
  } else {
    // const _shouldntHappen: never = price;
    // throw new Error("This shouldnt have happend");
    exhaustivenessCheck(price);
    // var pancakes = 123 // Unreachable code detected
  }
}

function exhaustivenessCheck(value: never): never {
  throw new Error("This shouldnt have happend"); // never returns
  // while(true){} // never returns
}
