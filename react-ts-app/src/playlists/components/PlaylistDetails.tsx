// tsrafc
import React from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist?: Playlist;
  onEdit: () => void;
}

export const PlaylistDetails = ({ playlist, onEdit }: Props) => {
  if (!playlist)
    return <p className="alert alert-info">No playlist selected</p>;

  return (
    <div>
      PlaylistDetails
      <dl>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>

        <dt>Public:</dt>
        <dd style={{ color: playlist.public ? "red" : "green" }}>
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>
      <button className="btn btn-info mt-3" onClick={onEdit}>
        Edit
      </button>
    </div>
  );
};
