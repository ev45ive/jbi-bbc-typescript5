

## Lowercase types for primitives
```ts
const n: Number = Number("12");
// n + 2; // Operator '+' cannot be applied to types 'Number' and 'number'.
```

## Type Assertions

```ts

let x :Playlist;
(x as object) // downcast to more general type

let x :number;

// Force type:
(x as unknown as string).toLocaleLowerCase()

```



## Literal types

```ts

let variable1: string = '123'
let variable2: "A"|"B"|"C" = 'B'
variable1 = variable2
// variable2 = variable1 // error 
```