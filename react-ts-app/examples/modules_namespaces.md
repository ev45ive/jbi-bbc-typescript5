
```ts

(function (exports = {}){
    var priv = 1
    var getter = function getVal(){ return priv }
    var setter = function setVal(v){ return priv = v}

    exports.getVal = getter;
    exports.setVal = setter;
    return exports 
})(window.mymod = {})

mymod.getVal()
// 1

mymod
// {getVal: ƒ, setVal: ƒ}

```