/**
 * Function that add numbers
 *
 * @export
 * @param {number} a
 * @param {number} b
 * @return {number} a result
 */
export function add(a: number, b: number): number;
export function substract(a: any, b: any): number;
export function multiply(a: any, b: any): number;
