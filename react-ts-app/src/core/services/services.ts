import { UserProfile } from "../model/UserProfile";
import { AlbumSearchService } from "./AlbumSearchService";
import { AuthService } from "./AuthService";
import { MockSearchService } from "./MockSearchService";
import { UserService } from "./UserService";

interface Entity { id: string, name: string }
export interface Album extends Entity {
    type: 'album'
    images: [
        { url: string }
    ]
}
interface Playlist extends Entity { type: 'playlist' }

export interface IProvideUser {
    // new(authService: IProvideAccessToken)
    // getUser() UserProfile // sync
    // getUser(cb: (res: UserProfile) => void): void // calback
    getUser(): Promise<UserProfile>
}

export interface IManageSession {
    login(): void
    logout(): void
}
export interface IProvideAccessToken {
    getAccessToken(): string
}

export interface ISearchAlbums {
    search(params: { query: string }): Promise<Album[]>
}


export const authService = new AuthService({
    client_id: 'fb3be86c2c104b8992d260d30259efe1',
    redirect_uri: 'http://localhost:3000/',
    scope: 'user-read-private user-read-email'
})
export const userService = new UserService(authService)
export const searchService = new AlbumSearchService()