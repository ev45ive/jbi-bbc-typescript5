
```ts

// # Literal types + const + as const

// "Hello World"
// const constantString = "Hello World";
// let constantString = "Hello World";
// let constantString:"Hello World" = "Hello World";
// let constantString = "Hello World" as "Hello World";
// let constantString = "Hello World" as "Hello World" | "abc";
let constantString = "Hello World" as const;

// This condition will always return 'false' since the types '"Hello World"' and '"abc"' have no overlap.
if(constantString == 'abc'){}

// constantString = 'abc' as "Hello World" // bad, but works..

let x = constantString as string
if(x == 'abc'){} // ok


```