import { IManageSession, IProvideAccessToken } from "./services";

export class AuthService implements IProvideAccessToken, IManageSession {

    private access_token = ''

    constructor(private config: {
        client_id: string,
        redirect_uri: string
        scope: string
    }) { }

    init(): void {
        // new token
        if (window.location.hash) {
            const access_token = new URLSearchParams(window.location.hash).get('#access_token')
            if (access_token) {
                sessionStorage.setItem('token', access_token)
                this.access_token = access_token
                window.location.hash  = ''
            }
        }

        // existing token
        if (this.access_token == '') {
            const access_token = sessionStorage.getItem('token')
            access_token && (this.access_token = access_token)
        }

        // no token
        if (this.access_token == '') {
            this.login()
        }
    }

    login(): void {
        const state = (1234567890).toString(); // random string ;=)

        localStorage.setItem('nonce', state);
        const scope = 'user-read-private user-read-email';

        const url = 'https://accounts.spotify.com/authorize'
            + '?response_type=token'
            + '&client_id=' + encodeURIComponent(this.config.client_id)
            + '&scope=' + encodeURIComponent(this.config.scope)
            + '&redirect_uri=' + encodeURIComponent(this.config.redirect_uri)
            + '&state=' + encodeURIComponent(state)

        window.location.href = (url);
    }

    logout(): void {
        throw new Error("Method not implemented.");
    }

    getAccessToken(): string {
        return this.access_token
    }

}
