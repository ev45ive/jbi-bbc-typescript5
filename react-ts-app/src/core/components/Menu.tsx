// Module as namespace
// import * as Menu from "../../core/components/Menu";
// Menu.Menu.List

import React, { ReactElement } from "react";

export namespace Menu {
  export interface MenuProps {
    children: ReactElement[];
  }
  export interface Props {}
  let privateVar = 123;

  export namespace Options {
    var subprivate = 123;

    export const setOption = (privateOPt: any) => {
      privateVar = privateOPt;
    };
  }
  //   subprivate // Cannot find name 'subprivate'.

  export const List = ({}: MenuProps) => {
    return <div></div>;
  };

  export const Item = (props: Props) => {
    return <div></div>;
  };
}


// type x = JSX.IntrinsicElements['div']
