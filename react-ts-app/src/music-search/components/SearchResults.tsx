import React from "react";
import { Artist } from "../../core/model/Artist";
import { Album } from "../../core/services/services";
import { AlbumCard } from "./AlbumCard";
import { ArtistCard } from "./ArtistCard";

interface Props {
  results: (Album | Artist)[];
}

export const SearchResults = ({ results }: Props) => {
  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 g-0">
        {results.map((result) => {
          return (
            <div className="col" key={result.id}>
              {result.type == "album" && <AlbumCard result={result} />}
              {result.type == "artist" && <ArtistCard result={result} />}
            </div>
          );
        })}
      </div>
    </div>
  );
};
