```ts
function Person(name){
    this.name  = name; 
}

p = new Person()
Person {name: undefined}name: undefined
    [[Prototype]]: Object
        constructor: ƒ Person(name)
            [[Prototype]]: Object

p instanceof Person 
true

p.__proto__.constructor

ƒ Person(name){
    this.name  = name; 
}

p.__proto__.constructor === Person 
true

class Employee extends Person {} 
undefined

e = new Employee()
Employee {name: undefined}

e instanceof Employee
true

e instanceof Person 
true

e 
Employee {name: undefined} 
[[Prototype]]:
    constructor: class Employee
    [[Prototype]]: Object
        constructor: ƒ Person(name)
        [[Prototype]]: Object

person = {name:'Alice'} 

{name: 'Alice'}
[[Prototype]]: Object