
// declare global {
declare interface JQueryStatic {
    ajax(x: 1): void
    myPlugin(x: number): void
}
// }

// declare module 'jquery' {
//     export default {
//         /** 
//                My jQueryPlugin
//            */
//         myPlugin(): void
//     }
// }
